FROM openjdk:11.0.10-jre-buster
ENV KEYMAKER_PORT=80
COPY ./target/keymaker-*.jar /app.jar
ENTRYPOINT ["sh", "-c", "SENTRY_ENVIRONMENT=$ENV java -Djava.security.egd=file:/dev/./urandom -jar /app.jar"]
HEALTHCHECK --start-period=20s \
  CMD wget -O - http://localhost:80/actuator/health | egrep -o '^{"status":"UP"'

package org.crossref.keymaker.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.Called
import io.mockk.Runs
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.just
import io.mockk.verify
import org.crossref.keymaker.service.UserAdminService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@WebMvcTest(UserAdminController::class)
class UserAdminControllerTests(@Autowired val mockMvc: MockMvc, @Autowired val mapper: ObjectMapper) {
    @MockkBean
    lateinit var userAdminService: UserAdminService

    @BeforeEach
    fun `reset mocks`() {
        clearAllMocks()
    }

    @Test
    fun `unfiltered user search with valid token returns all users`() {
        every { userAdminService.findUsers(emptyMap()) } returns users

        mockMvc.perform(
            MockMvcRequestBuilders.get("/keymaker/api/v1/user").with(mockToken("ROLE_STAFF"))
        ).andExpect(MockMvcResultMatchers.status().isOk).andExpect(
            MockMvcResultMatchers.content()
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(users)))
    }

    @Test
    fun `filtered user search with valid token returns filtered users`() {
        val filteredUsers = listOf(users[0])
        every {
            userAdminService.findUsers(
                mapOf(
                    "userId" to users[0].userId.toString(),
                    "username" to "one@user.org"
                )
            )
        } returns filteredUsers

        mockMvc.perform(
            MockMvcRequestBuilders.get("/keymaker/api/v1/user")
                .with(mockToken("ROLE_STAFF"))
                .param("userId", users[0].userId.toString())
                .param("username", "one@user.org")
        ).andExpect(MockMvcResultMatchers.status().isOk).andExpect(
            MockMvcResultMatchers.content()
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(filteredUsers)))
    }

    @Test
    fun `controller handles AuthServiceNotAvailableException`() {
        val ex = AuthServiceUnavailableException("HttpHostConnectException: localhost:8080: connection refused")
        every { userAdminService.findUsers(any()) } throws ex

        mockMvc.perform(
            MockMvcRequestBuilders.get("/keymaker/api/v1/user").with(mockToken("ROLE_STAFF"))
        ).andExpect(MockMvcResultMatchers.status().isServiceUnavailable).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                MockMvcResultMatchers.content().json(
                    mapper.writeValueAsString(
                        ResponseMessage(
                            HttpStatus.SERVICE_UNAVAILABLE, "Authorization service unavailable", listOf(ex.message)
                        )
                    )
                )
            )
    }

    @Test
    fun `user search with missing token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.get("/keymaker/api/v1/user")
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized)

        verify { userAdminService wasNot Called }
    }

    @Test
    fun `user search with unauthorized token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.get("/keymaker/api/v1/user").with(mockToken())
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { userAdminService wasNot Called }
    }

    @Test
    fun `get user with valid token returns single user`() {
        every { userAdminService.getUser(users[0].userId!!) } returns users[0]

        mockMvc.perform(
            MockMvcRequestBuilders.get("/keymaker/api/v1/user/${users[0].userId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(MockMvcResultMatchers.status().isOk).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(users[0])))
    }

    @Test
    fun `controller handles UserNotFoundException`() {
        every { userAdminService.getUser(users[0].userId!!) } throws UserNotFoundException(users[0].userId!!)

        mockMvc.perform(
            MockMvcRequestBuilders.get("/keymaker/api/v1/user/${users[0].userId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(MockMvcResultMatchers.status().isNotFound).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                MockMvcResultMatchers.content().json(
                    mapper.writeValueAsString(
                        ResponseMessage(
                            HttpStatus.NOT_FOUND, "User with ID ${users[0].userId} not found", emptyList()
                        )
                    )
                )
            )
    }

    @Test
    fun `get user with missing token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.get("/keymaker/api/v1/user/${users[0].userId}")
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized)

        verify { userAdminService wasNot Called }
    }

    @Test
    fun `get user with unauthorized token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.get("/keymaker/api/v1/user/${users[0].userId}").with(mockToken())
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { userAdminService wasNot Called }
    }

    @Test
    fun `add user with valid token completes successfully`() {
        every { userAdminService.addUser(users[0]) } returns users[0]
        mockMvc.perform(
            MockMvcRequestBuilders.post("/keymaker/api/v1/user").with(mockToken("ROLE_STAFF")).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(users[0]))
        ).andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(users[0])))
    }

    @Test
    fun `controller handles UserAlreadyExistsException`() {
        every { userAdminService.addUser(users[0]) } throws UserAlreadyExistsException(
            users[0].userId!!,
            users[0].username
        )

        mockMvc.perform(
            MockMvcRequestBuilders.post("/keymaker/api/v1/user").with(mockToken("ROLE_STAFF")).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(users[0]))
        ).andExpect(MockMvcResultMatchers.status().isConflict).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                MockMvcResultMatchers.content().json(
                    mapper.writeValueAsString(
                        ResponseMessage(
                            HttpStatus.CONFLICT,
                            "User with username ${users[0].username} already exists with ID ${users[0].userId}",
                            emptyList()
                        )
                    )
                )
            )
    }

    @Test
    fun `add user with missing token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.post("/keymaker/api/v1/user")
        ).andExpect(MockMvcResultMatchers.status().isForbidden)

        verify { userAdminService wasNot Called }
    }

    @Test
    fun `add user with unauthorized token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.post("/keymaker/api/v1/user").with(mockToken()).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(users[0]))
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { userAdminService wasNot Called }
    }

    @Test
    fun `update user with valid token completes successfully`() {
        every { userAdminService.updateUser(users[0]) } just Runs
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/user/${users[0].userId}").with(mockToken("ROLE_STAFF"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(users[0]))
        ).andExpect(MockMvcResultMatchers.status().isNoContent)

        verify { userAdminService.updateUser(users[0]) }
    }

    @Test
    fun `update user with missing token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/user/${users[0].userId}")
        ).andExpect(MockMvcResultMatchers.status().isForbidden)

        verify { userAdminService wasNot Called }
    }

    @Test
    fun `update user with unauthorized token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/user/${users[0].userId}").with(mockToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(users[0]))
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { userAdminService wasNot Called }
    }

    @Test
    fun `delete user with valid token completes successfully`() {
        every { userAdminService.deleteUser(users[0].userId!!) } just Runs
        mockMvc.perform(
            MockMvcRequestBuilders.delete("/keymaker/api/v1/user/${users[0].userId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(MockMvcResultMatchers.status().isNoContent)

        verify { userAdminService.deleteUser(users[0].userId!!) }
    }

    @Test
    fun `delete user with missing token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.delete("/keymaker/api/v1/user/${users[0].userId}")
        ).andExpect(MockMvcResultMatchers.status().isForbidden)

        verify { userAdminService wasNot Called }
    }

    @Test
    fun `delete user with unauthorized token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.delete("/keymaker/api/v1/user/${users[0].userId}").with(mockToken())
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { userAdminService wasNot Called }
    }
}

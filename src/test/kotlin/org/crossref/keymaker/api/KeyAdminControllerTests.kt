package org.crossref.keymaker.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.Called
import io.mockk.Runs
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.just
import io.mockk.verify
import org.crossref.keymaker.service.KeyAdminService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@WebMvcTest(KeyAdminController::class)
class KeyAdminControllerTests(@Autowired val mockMvc: MockMvc, @Autowired val mapper: ObjectMapper) {
    @MockkBean
    lateinit var keyAdminService: KeyAdminService

    @BeforeEach
    fun `reset mocks`() {
        clearAllMocks()
    }

    @Test
    fun `add key with valid token completes successfully`() {
        every { keyAdminService.addKey(any(), any(), any()) } returns KeyToken(keys[0], "token")
        mockMvc.perform(
            MockMvcRequestBuilders.post("/keymaker/api/v1/key/${orgs[0].orgId}")
                .with(mockToken("ROLE_STAFF", "ROLE_USER")).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(keys[0]))
        ).andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(KeyToken(keys[0], "token"))))
    }

    @Test
    fun `controller handles OrgNotFoundException`() {
        every {
            keyAdminService.addKey(
                orgs[0].orgId!!,
                any(),
                keys[0]
            )
        } throws OrgNotFoundException(orgs[0].orgId!!)

        mockMvc.perform(
            MockMvcRequestBuilders.post("/keymaker/api/v1/key/${orgs[0].orgId}")
                .with(mockToken("ROLE_USER")).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(keys[0]))
        ).andExpect(MockMvcResultMatchers.status().isNotFound).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.NOT_FOUND,
                        "Organization with ID ${orgs[0].orgId} not found",
                        emptyList()
                    )
                )
            )
        )
    }

    @Test
    fun `add key with missing token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.post("/keymaker/api/v1/key/${orgs[0].orgId}").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(keys[0]))
        ).andExpect(MockMvcResultMatchers.status().isForbidden)

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `add key with unauthorized token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.post("/keymaker/api/v1/key/${orgs[0].orgId}")
                .with(mockToken()).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(keys[0]))
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `delete key with valid token completes successfully`() {
        every { keyAdminService.deleteKey(keys[0].keyId!!, any()) } just Runs
        mockMvc.perform(
            MockMvcRequestBuilders.delete("/keymaker/api/v1/key/${keys[0].keyId}").with(mockToken("ROLE_USER"))
        ).andExpect(MockMvcResultMatchers.status().isNoContent)

        verify { keyAdminService.deleteKey(keys[0].keyId!!, any()) }
    }

    @Test
    fun `delete key with missing token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.delete("/keymaker/api/v1/key/${keys[0].keyId}")
        ).andExpect(MockMvcResultMatchers.status().isForbidden)

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `delete key with unauthorized token fails as expected`() {
        mockMvc.perform(
            MockMvcRequestBuilders.delete("/keymaker/api/v1/key/${keys[0].keyId}").with(mockToken())
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `controller handles KeyNotFoundException`() {
        every {
            keyAdminService.deleteKey(
                keys[0].keyId!!, any()
            )
        } throws KeyNotFoundException(keys[0].keyId!!)

        mockMvc.perform(
            MockMvcRequestBuilders.delete("/keymaker/api/v1/key/${keys[0].keyId}")
                .with(mockToken("ROLE_USER"))
        ).andExpect(MockMvcResultMatchers.status().isNotFound).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.NOT_FOUND,
                        "Key with ID ${keys[0].keyId} not found",
                        emptyList()
                    )
                )
            )
        )
    }

    @Test
    fun `enable key with staff token token works`() {
        every { keyAdminService.setKeyEnabledStatus(keys[0].keyId!!, true) } just Runs
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}/enable").with(mockToken("ROLE_STAFF"))
        ).andExpect(MockMvcResultMatchers.status().isNoContent)

        verify { keyAdminService.setKeyEnabledStatus(keys[0].keyId!!, true) }
    }

    @Test
    fun `enable key with user token fails`() {
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}/enable").with(mockToken("ROLE_USER"))
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `enable key with invalid token fails`() {
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}/enable").with(mockToken())
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `enable key with missing token fails`() {
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}/enable")
        ).andExpect(MockMvcResultMatchers.status().isForbidden)

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `disable key with staff token token works`() {
        every { keyAdminService.setKeyEnabledStatus(keys[0].keyId!!, false) } just Runs
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}/disable").with(mockToken("ROLE_STAFF"))
        ).andExpect(MockMvcResultMatchers.status().isNoContent)

        verify { keyAdminService.setKeyEnabledStatus(keys[0].keyId!!, false) }
    }

    @Test
    fun `disable key with user token fails`() {
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}/disable").with(mockToken("ROLE_USER"))
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `disable key with invalid token fails`() {
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}/disable").with(mockToken())
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `disable key with missing token fails`() {
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}/disable")
        ).andExpect(MockMvcResultMatchers.status().isForbidden)

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `update key with valid user token completes successfully`() {
        every { keyAdminService.updateKey(keys[0], any()) } just Runs
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}")
                .with(mockToken("ROLE_USER")).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(keys[0]))
        ).andExpect(MockMvcResultMatchers.status().isNoContent)

        verify { keyAdminService.updateKey(keys[0], any()) }
    }

    @Test
    fun `update key with invalid token fails`() {
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}")
                .with(mockToken()).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(keys[0]))
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `update key with missing token fails`() {
        mockMvc.perform(
            MockMvcRequestBuilders.put("/keymaker/api/v1/key/${keys[0].keyId}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(keys[0]))
        ).andExpect(MockMvcResultMatchers.status().isForbidden)

        verify { keyAdminService wasNot Called }
    }
}

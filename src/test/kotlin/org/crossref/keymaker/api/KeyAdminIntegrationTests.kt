package org.crossref.keymaker.api

import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import java.time.LocalDateTime
import java.util.UUID

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
    properties = ["server.port=9191", "management.server.port=9042"]
)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class KeyAdminIntegrationTests : IntegrationTester() {
    companion object {
        val keys: MutableMap<String, Key> = mutableMapOf()
        val orgs: MutableMap<String, Organization> = mutableMapOf()
        val users: MutableMap<String, User> = mutableMapOf()
    }

    private inline fun <reified T : Any> doReq(method: String, path: String, token: String, data: Any? = null): Pair<Int, T> =
        with(prepareReq(method, path, token, data).response().second) {
            Pair<Int, T> (this.statusCode, mapper.readValue<T>(this.data))
        }

    @Test
    @Order(10)
    fun `Obtaining keys for organization returns empty list`() {
        doReq<List<Organization>>(
            "GET",
            "/keymaker/api/v1/organization?name=org3",
            staffToken
        ).second.also {
            orgs["org3"] = it.first()
        }

        doReq<List<Key>>(
            "GET",
            "/keymaker/api/v1/organization/${orgs["org3"]!!.orgId}/keys",
            staffToken
        ).second.also {
            assertTrue(it.isEmpty(), "Organization should have no keys")
        }
    }

    @Test
    @Order(20)
    fun `Obtaining keys for wrong organization returns not found`() {
        doReq<ResponseMessage>(
            "GET",
            "/keymaker/api/v1/organization/${UUID.randomUUID()}/keys",
            staffToken
        ).also {
            checkStatus(
                it, HttpStatus.NOT_FOUND,
                ".*Organization.*"
            )
        }
    }

    @Test
    @Order(30)
    fun `Add a key to an organization works`() {
        doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/user?username=test@user.com",
            staffToken
        ).also {
            users["user1"] = it.second.first()
        }

        execReq(
            "POST",
            "/keymaker/api/v1/organization/${orgs["org3"]!!.orgId}/admins/${users["user1"]!!.userId}",
            staffToken
        )

        doReq<KeyToken>(
            "POST",
            "/keymaker/api/v1/key/${orgs["org3"]!!.orgId}",
            userToken,
            Key(UUID.randomUUID(), "description1", UUID.randomUUID(), LocalDateTime.now(), false)
        ).also {
            keys["key1"] = it.second.key
            assertEquals("description1", it.second.key.description)
        }
    }

    @Test
    @Order(40)
    fun `Add a key to a wrong organization fails`() {
        doReq<ResponseMessage>(
            "POST",
            "/keymaker/api/v1/key/${UUID.randomUUID()}",
            userToken,
            Key(UUID.randomUUID(), "description1", UUID.randomUUID(), LocalDateTime.now(), false)
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                ".*Organization.*"
            )
        }
    }

    @Test
    @Order(50)
    fun `List keys for your organization works`() {
        doReq<List<Key>>(
            "GET",
            "/keymaker/api/v1/organization/${orgs["org3"]!!.orgId}/keys",
            userToken
        ).also {
            assertEquals("description1", it.second.first().description)
            assertTrue(it.second.isNotEmpty())
        }
    }

    @Test
    @Order(60)
    fun `List keys for not your organization fails`() {
        val (_, listOrgs) = doReq<kotlin.collections.List<org.crossref.keymaker.api.Organization>>(
            "GET",
            "/keymaker/api/v1/organization?name=org2",
            staffToken
        )
        orgs["org2"] = listOrgs.first()

        doReq<ResponseMessage>(
            "GET",
            "/keymaker/api/v1/organization/${orgs["org2"]!!.orgId}/keys",
            userToken
        ).also {
            checkStatus(
                it,
                HttpStatus.CONFLICT,
                ".*is not an administrator of organization with ID.*"
            )
        }
    }

    @Test
    @Order(70)
    fun `List keys as staff works for an organization not belonged to`() {
        doReq<List<Key>>(
            "GET",
            "/keymaker/api/v1/organization/${orgs["org3"]!!.orgId}/keys",
            staffToken
        ).also {
            assertEquals("description1", it.second.first().description)
            assertTrue(it.second.isNotEmpty())
        }
    }

    @Test
    @Order(80)
    fun `Staff doesn't have special permissions to add keys`() {
        doReq<ResponseMessage>(
            "POST",
            "/keymaker/api/v1/key/${orgs["org3"]!!.orgId}",
            staffToken,
            Key(UUID.randomUUID(), "description1", UUID.randomUUID(), LocalDateTime.now(), false)
        ).also {
            checkStatus(
                it,
                HttpStatus.CONFLICT,
                ".*is not an administrator of organization with ID.*"
            )
        }
    }

    @Test
    @Order(120)
    fun `Staff can disable key`() {
        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/key/${keys["key1"]!!.keyId}/disable",
                staffToken
            ).statusCode
        )

        doReq<List<Key>>(
            "GET",
            "/keymaker/api/v1/organization/${orgs["org3"]!!.orgId}/keys",
            userToken
        ).also {
            assertTrue(!it.second.filter { it.keyId == keys["key1"]!!.keyId }.first().enabled)
        }
    }

    @Test
    @Order(125)
    fun `Re-disabling throws exception`() {
        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/key/${keys["key1"]!!.keyId}/disable",
            staffToken
        ).also {
            checkStatus(it, HttpStatus.CONFLICT, ".*is already disabled.*")
        }
    }

    @Test
    @Order(130)
    fun `Staff can enable key`() {
        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/key/${keys["key1"]!!.keyId}/enable",
                staffToken
            ).statusCode
        )

        doReq<List<Key>>(
            "GET",
            "/keymaker/api/v1/organization/${orgs["org3"]!!.orgId}/keys",
            userToken
        ).also {
            assertTrue(it.second.filter { it.keyId == keys["key1"]!!.keyId }.first().enabled)
        }
    }

    @Test
    @Order(140)
    fun `Re-enabling throws exception`() {
        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/key/${keys["key1"]!!.keyId}/enable",
            staffToken
        ).also {
            checkStatus(it, HttpStatus.CONFLICT, ".*is already enabled.*")
        }
    }

    @Test
    @Order(150)
    fun `Non staff cannot change key enabled status`() {
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/key/${keys["key1"]!!.keyId}/disable",
                userToken
            ).statusCode
        )
    }

    @Test
    @Order(155)
    fun `enabling random key returns not found`() {
        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/key/${UUID.randomUUID()}/enable",
            staffToken
        ).also {
            checkStatus(it, HttpStatus.NOT_FOUND, ".*Key.*")
        }
    }

    @Test
    @Order(160)
    fun `non administrator cannot change key description`() {
        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/key/${keys["key1"]!!.keyId}",
            staffToken,
            Key(UUID.randomUUID(), "description2", UUID.randomUUID(), LocalDateTime.now(), false)
        ).also {
            checkStatus(it, HttpStatus.CONFLICT, ".*is not an administrator.*")
        }
    }

    @Test
    @Order(170)
    fun `change random key description returns not found`() {
        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/key/${UUID.randomUUID()}",
            userToken,
            Key(UUID.randomUUID(), "description2", UUID.randomUUID(), LocalDateTime.now(), false)
        ).also {
            checkStatus(it, HttpStatus.NOT_FOUND, ".*Key.*")
        }
    }

    @Test
    @Order(180)
    fun `Administrator can change key description`() {
        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/key/${keys["key1"]!!.keyId}",
                userToken,
                Key(UUID.randomUUID(), "description2", UUID.randomUUID(), LocalDateTime.now(), false)
            ).statusCode
        )

        doReq<List<Key>>(
            "GET",
            "/keymaker/api/v1/organization/${orgs["org3"]!!.orgId}/keys",
            userToken
        ).also {
            assertEquals("description2", it.second.filter { it.keyId == keys["key1"]!!.keyId }.first().description)
        }
    }

    @Test
    @Order(990)
    fun `Delete Keys for a non org admin fails`() {
        assertEquals(
            HttpStatus.CONFLICT.value(),
            execReq(
                "DELETE",
                "/keymaker/api/v1/key/${keys["key1"]!!.keyId}",
                staffToken,
            ).statusCode
        )
    }

    @Test
    @Order(1000)
    fun `Delete Keys`() {
        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "DELETE",
                "/keymaker/api/v1/key/${keys["key1"]!!.keyId}",
                userToken,
            ).statusCode
        )
    }

    @Test
    @Order(1010)
    fun `Delete Keys for non existent key returns 404`() {
        doReq<ResponseMessage>(
            "DELETE",
            "/keymaker/api/v1/key/${keys["key1"]!!.keyId}",
            userToken,
        ).also {
            checkStatus(it, HttpStatus.NOT_FOUND, ".*Key.*")
        }

        execReq(
            "DELETE",
            "/keymaker/api/v1/organization/${orgs["org3"]!!.orgId}/admins/${users["user1"]!!.userId}",
            staffToken
        )
    }
}

package org.crossref.keymaker.api

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.httpDelete
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpPut
import org.junit.jupiter.api.Assertions
import org.keycloak.admin.client.KeycloakBuilder
import org.springframework.http.HttpStatus
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt
import java.time.LocalDateTime
import java.util.UUID

val orgs = listOf(
    Organization(UUID.randomUUID(), "Org 1", 1, "one@org.org", "First organization"),
    Organization(UUID.randomUUID(), "Org 2", 2, "two@org.org", "Second organization"),
    Organization(UUID.randomUUID(), "Org 3", 3, "three@org.org", "Third organization")
)

val users = listOf(
    User(UUID.randomUUID(), "one@user.org", "User", "One"),
    User(UUID.randomUUID(), "two@user.org", "User", "Two"),
    User(UUID.randomUUID(), "three@user.org", "User", "Three")
)

val keys = listOf(
    Key(UUID.randomUUID(), "desc1", users[0].userId!!, LocalDateTime.now(), true),
    Key(UUID.randomUUID(), "desc2", users[1].userId!!, LocalDateTime.now(), true),
    Key(UUID.randomUUID(), "desc3", users[2].userId!!, LocalDateTime.now(), true)
)

fun mockToken(vararg authorities: String): JwtRequestPostProcessor =
    jwt().jwt(Jwt.withTokenValue("asd").header("typ", "JWT").claim("sub", UUID.randomUUID()).build())
        .authorities(authorities.map { authority -> SimpleGrantedAuthority(authority) })

abstract class IntegrationTester {
    companion object {
        val serverUrl = System.getenv("KEYCLOAK_ADMIN_URL") ?: "http://localhost:8080/auth"

        fun getKeycloakToken(username: String, password: String): String = KeycloakBuilder.builder()
            .serverUrl(serverUrl)
            .realm("crossref")
            .clientId("mycrossref")
            .username(username)
            .password(password)
            .build().tokenManager().accessTokenString

        val staffToken = getKeycloakToken("cdelojo@crossref.org", "pwd")
        var userToken = getKeycloakToken("test@user.com", "pwd")
        val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())
    }

    fun prepareReq(method: String, path: String, token: String, data: Any? = null) = with("http://localhost:9191$path") {
        when (method) {
            "GET" -> this.httpGet()
            "POST" -> this.httpPost()
            "PUT" -> this.httpPut()
            "DELETE" -> this.httpDelete()
            else -> throw Exception("method not supported")
        }.header("Authorization", "Bearer $token").also { if (data != null) { if (data is String) it.body(data) else it.jsonBody(mapper.writeValueAsString(data)) } }
    }

    fun execReq(method: String, path: String, token: String, data: Any? = null) =
        prepareReq(method, path, token, data).response().second

    fun checkStatus(p: Pair<Int, ResponseMessage>, s: HttpStatus, messagePattern: String? = null) {
        Assertions.assertEquals(s, p.second.status, "Wrong status message, it should be $s")
        Assertions.assertEquals(s.value(), p.first, "Wrong HTTP status code, it should be ${s.value()}")
        messagePattern?.let {
            Assertions.assertTrue(
                messagePattern.toRegex().matches(p.second.message),
                "Message [${p.second.message}] does not match regex `$messagePattern`"
            )
        }
    }
}

package org.crossref.keymaker.service

import org.crossref.keymaker.api.Key
import org.crossref.keymaker.api.KeyToken
import java.util.UUID

interface KeyAdminService {
    fun findKeys(orgId: UUID): List<Key>
    fun findKeys(orgId: UUID, userId: UUID): List<Key>
    fun addKey(orgId: UUID, userId: UUID, key: Key): KeyToken
    fun deleteKey(keyId: UUID, userId: UUID)
    fun setKeyEnabledStatus(keyId: UUID, enabled: Boolean)
    fun updateKey(key: Key, userId: UUID)
}

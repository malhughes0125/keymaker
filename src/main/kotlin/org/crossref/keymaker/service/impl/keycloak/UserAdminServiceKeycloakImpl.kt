package org.crossref.keymaker.service.impl.keycloak

import org.crossref.keymaker.api.User
import org.crossref.keymaker.service.UserAdminService
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class UserAdminServiceKeycloakImpl(private val kc: KeycloakConnector) : UserAdminService {

    override fun findUsers(searchParams: Map<String, String>): List<User> =
        kc.findUsers(
            searchParams.getOrDefault("username", ""),
            searchParams.getOrDefault("firstName", ""),
            searchParams.getOrDefault("lastName", "")
        )

    override fun addUser(user: User): User = kc.addUser(user)

    override fun getUser(userId: UUID): User = kc.getUser(userId)

    override fun updateUser(user: User) {
        kc.updateUser(user)
    }

    override fun deleteUser(userId: UUID) {
        kc.deleteUser(userId)
    }
}

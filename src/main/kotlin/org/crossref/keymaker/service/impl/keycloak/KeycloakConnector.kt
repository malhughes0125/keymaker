package org.crossref.keymaker.service.impl.keycloak

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.nimbusds.jwt.JWTParser
import org.apache.commons.validator.routines.EmailValidator
import org.crossref.keymaker.api.AuthServiceUnavailableException
import org.crossref.keymaker.api.InvalidOrgNameException
import org.crossref.keymaker.api.InvalidUsernameException
import org.crossref.keymaker.api.Key
import org.crossref.keymaker.api.KeyNotFoundException
import org.crossref.keymaker.api.KeyStatusUnchangedException
import org.crossref.keymaker.api.KeyToken
import org.crossref.keymaker.api.OrgAlreadyExistsException
import org.crossref.keymaker.api.OrgNotFoundException
import org.crossref.keymaker.api.OrgSubscriptionUnchangedException
import org.crossref.keymaker.api.Organization
import org.crossref.keymaker.api.Subscription
import org.crossref.keymaker.api.User
import org.crossref.keymaker.api.UserAlreadyExistsException
import org.crossref.keymaker.api.UserAlreadyOrgAdminException
import org.crossref.keymaker.api.UserNotFoundException
import org.crossref.keymaker.api.UserNotOrgAdminException
import org.keycloak.admin.client.CreatedResponseUtil
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.admin.client.resource.RealmResource
import org.keycloak.representations.idm.CredentialRepresentation
import org.keycloak.representations.idm.GroupRepresentation
import org.keycloak.representations.idm.RoleRepresentation
import org.keycloak.representations.idm.UserRepresentation
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.Date
import java.util.UUID
import javax.ws.rs.NotFoundException
import javax.ws.rs.ProcessingException
import javax.ws.rs.core.Response

class KeycloakConnector(val url: String, val realm: String, user: String, password: String) {
    private val keycloak: Keycloak
    val realmResource: RealmResource
    val userRoleRep: RoleRepresentation
    val staffRoleRep: RoleRepresentation
    val apikeyRoleRep: RoleRepresentation
    private val mapper = jacksonObjectMapper()
    val emailValidator: EmailValidator = EmailValidator.getInstance()
    val logger: Logger = LoggerFactory.getLogger(this::class.java)!!

    @JsonAutoDetect
    data class LongLivedTokenResponse(
        @JsonProperty("access_token") var access_token: String,
        @JsonProperty("expires_in") var expires: Int,
        @JsonProperty("not-before-policy") var not_before_policy: Int,
        @JsonProperty("refresh_expires_in") var refresh_expires_in: Int,
        @JsonProperty("scope") var scope: String,
        @JsonProperty("session_state") var session_state: String,
        @JsonProperty("token_type") var token_type: String
    )

    init {
        keycloak = KeycloakBuilder.builder()
            .serverUrl(url)
            .realm(realm)
            .clientId("admin-cli")
            .username(user)
            .password(password)
            .build()
        realmResource = keycloak.realm(realm)

        userRoleRep = realmResource.roles().get("ROLE_USER").toRepresentation()
        staffRoleRep = realmResource.roles().get("ROLE_STAFF").toRepresentation()
        apikeyRoleRep = realmResource.roles().get("ROLE_APIKEY").toRepresentation()
    }

    fun UserRepresentation.toKey(): Key {
        return Key(
            UUID.fromString(this.id),
            this.attributes.getOrDefault("description", listOf(""))[0],
            UUID.fromString(this.attributes.get("issuedBy")!![0]),
            LocalDateTime.ofEpochSecond(this.createdTimestamp / 1000, 0, ZoneOffset.UTC),
            this.isEnabled
        )
    }

    fun UserRepresentation.toUser(): User {
        return User(UUID.fromString(this.id), this.username, this.firstName, this.lastName)
    }

    fun GroupRepresentation.toOrg(): Organization {
        val sugarId = (this.attributes?.get("sugarId") ?: listOf("-1"))[0]
        val contact = (this.attributes?.get("contact") ?: listOf(""))[0]
        val description = (this.attributes?.get("description") ?: listOf(""))[0]
        val subscriptions = (this.attributes?.get("subscriptions") ?: listOf("[]"))[0]

        return Organization(
            UUID.fromString(this.id),
            this.name,
            sugarId.toLong(),
            contact,
            description,
            mapper.readValue(subscriptions, List::class.java)
                .map { Subscription.valueOf(it.toString()) } as MutableList<Subscription>
        )
    }

    fun Organization.toGroupRepresentation(): GroupRepresentation {
        val grp = GroupRepresentation()
        grp.name = this.name
        grp.id = this.orgId.toString()
        grp.attributes = mutableMapOf<String, List<String>>()
        grp.attributes.set("sugarId", listOf("${this.sugarId}"))
        grp.attributes["contact"] = listOf<String>("${this.contact}")
        grp.attributes["description"] = listOf<String>("${this.description ?: ""}")
        grp.attributes["subscriptions"] = listOf(mapper.writeValueAsString(this.subscriptions))
        return grp
    }

    fun Organization.members() =
        realmResource.groups().group(this.orgId.toString()).members()

    /*
     KEY MANAGEMENT
     */

    fun createKey(orgId: UUID, userId: UUID, description: String): KeyToken {
        UserRepresentation().also {
            val username = "apikey_${UUID.randomUUID()}"
            it.username = username
            it.isEnabled = true
            it.attributes = mutableMapOf<String, List<String>>()
            it.attributes["issuedBy"] = mutableListOf(userId.toString())
            it.attributes["description"] = mutableListOf(description)

            val response = realmResource.users().create(it)
            if (response.status != 201) {
                throw Exception(response.statusInfo.reasonPhrase)
            }

            val keyId = CreatedResponseUtil.getCreatedId(response)
            val keyResource = realmResource.users().get(keyId).also {
                it.update(
                    with(it.toRepresentation()) {
                        this.username = "apikey_$keyId"
                        this
                    }
                )
            }

            keyResource.roles().realmLevel().add(listOf(apikeyRoleRep))

            try {
                keyResource.joinGroup(orgId.toString())
            } catch (e: NotFoundException) {
                keyResource.remove()
                throw OrgNotFoundException(orgId)
            }

            val passwordCred = CredentialRepresentation()
            val tempPasswd = UUID.randomUUID().toString() + "Aa!"
            passwordCred.isTemporary = false
            passwordCred.type = CredentialRepresentation.PASSWORD
            passwordCred.value = tempPasswd
            keyResource.resetPassword(passwordCred)

            val longLivedToken = getLongLivedToken(username, tempPasswd)

            keyResource.removeCredential(keyResource.credentials()[0].id)

            return KeyToken(getKey(UUID.fromString(keyId)), longLivedToken)
        }
    }

    fun setKeyEnabled(keyId: UUID, enabledStatus: Boolean) {
        getKeyRepresentation(keyId).also {
            it.isEnabled == enabledStatus && throw KeyStatusUnchangedException(keyId, enabledStatus)
            it.isEnabled = enabledStatus
            realmResource.users().get(keyId.toString()).update(it)
        }
    }

    fun getKeysForOrg(orgId: UUID) = getOrg(orgId)
        .members()
        .filter { it.username.startsWith("apikey_") }
        .map { it.toKey() }

    fun updateKey(key: Key) {
        key.keyId?.let {
            getKeyRepresentation(key.keyId).also {
                it.attributes["description"] = mutableListOf(key.description)
                realmResource.users().get(key.keyId.toString()).update(it)
            }
        }
    }

    fun getKeyRepresentation(userId: UUID) =
        try {
            getUserRepresentation(userId)
        } catch (e: UserNotFoundException) {
            throw KeyNotFoundException(userId)
        }

    fun getKey(userId: UUID) =
        getKeyRepresentation(userId).toKey()

    fun deleteKey(keyId: UUID) {
        try {
            deleteUser(keyId)
        } catch (e: UserNotFoundException) {
            throw KeyNotFoundException(keyId)
        }
    }

    /*
      ORGANIZATION MANAGEMENT
     */

    fun getOrgs() = getOrgsGroup().subGroups.map { it.toOrg() }

    fun getOrgRepresentation(orgId: UUID) =
        try {
            realmResource.groups().group(orgId.toString()).toRepresentation()
        } catch (e: NotFoundException) {
            throw OrgNotFoundException(orgId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }

    fun getOrg(orgId: UUID) =
        getOrgRepresentation(orgId).toOrg()

    fun getUsersForOrg(orgId: UUID) = getOrg(orgId)
        .members()
        .map { it.toUser() }
        .filterNot { it.username.startsWith("apikey_") }

    fun getAdminsForOrg(orgId: UUID): List<User> =
        getGroupAdminId(orgId).let {
            getOrg(UUID.fromString(it)).members().map { it.toUser() }
        }

    private fun getOrgsGroup(): GroupRepresentation {
        val orgquery = (realmResource.groups().groups("", 0, 999999, false).filter { it.name == "organizations" })

        if (orgquery.size != 1) {
            throw Exception("Group /organizations not found")
        }

        return orgquery[0]
    }

    fun addOrg(organization: Organization): Organization {
        "[a-z0-9_&()\\[\\]., -]+".toRegex().matches(organization.name) || throw InvalidOrgNameException()

        val grp = organization.toGroupRepresentation()
        grp.path = "/organizations/" + grp.name
        grp.id = null

        try {
            val g = realmResource.getGroupByPath(grp.path)
            throw OrgAlreadyExistsException(UUID.fromString(g.id), g.name, g.attributes.getOrDefault("sugarId", listOf("-1"))[0].toLong())
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        } catch (e: NotFoundException) { /* Empty because we only continue if the org has not been found */ }

        val adminGroup = GroupRepresentation()
        adminGroup.name = "admins"

        grp.id = addSubGroup(getOrgsGroup(), grp).toString()
        addSubGroup(grp, adminGroup)

        return grp.toOrg()
    }

    fun updateOrg(organization: Organization) {
        val grp = organization.toGroupRepresentation()
        organization.orgId?.let {
            val origGroup = realmResource.groups().group(it.toString())
            try {
                grp.attributes["subscriptions"] =
                    origGroup.toRepresentation().attributes.getOrDefault("subscriptions", listOf("[]"))
            } catch (e: NotFoundException) {
                throw OrgNotFoundException(it)
            } catch (e: ProcessingException) {
                throw AuthServiceUnavailableException(e.message)
            }
            origGroup.update(grp)
        }
    }

    fun deleteOrg(orgId: UUID) {
        try {
            realmResource.groups().group(orgId.toString()).remove()
        } catch (e: NotFoundException) {
            throw OrgNotFoundException(orgId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }
    }

    fun addOrgAdminUser(orgId: UUID, userId: UUID) {
        val adminsGroupId = getGroupAdminId(orgId)

        val user = realmResource.users().get(userId.toString())

        try {
            user.groups().any { it.id == adminsGroupId.toString() } && throw UserAlreadyOrgAdminException(orgId, userId)
            user.joinGroup(orgId.toString())
            user.joinGroup(adminsGroupId.toString())
        } catch (e: NotFoundException) {
            throw UserNotFoundException(userId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }
    }

    fun removeOrgAdminUser(orgId: UUID, userId: UUID) {
        val adminsGroupId = getGroupAdminId(orgId)

        try {
            val user = realmResource.users().get(userId.toString())
            user.groups().all { it.id != adminsGroupId.toString() } && throw UserNotOrgAdminException(orgId, userId)
            user.leaveGroup(orgId.toString())
            user.leaveGroup(adminsGroupId.toString())
        } catch (e: NotFoundException) {
            throw UserNotFoundException(userId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }
    }

    fun setOrgSubscriptionStatus(orgId: UUID, subscription: Subscription, status: Boolean) {
        val org = getOrg(orgId)
        if (status && !org.subscriptions.contains(subscription))
            org.subscriptions.add(subscription)
        else if (!status && org.subscriptions.contains(subscription))
            org.subscriptions.remove(subscription)
        else
            throw OrgSubscriptionUnchangedException(orgId, subscription, status)

        val grp = org.toGroupRepresentation()
        org.orgId?.let { realmResource.groups().group(it.toString()).update(grp) }
    }

    /*
    USER MANAGEMENT
    */

    fun findUsers(username: String, firstName: String, lastName: String): List<User> =
        realmResource.users().search(username, firstName, lastName, "", 0, 1000).map { it.toUser() }

    fun addUser(user: User): User {
        val ur = UserRepresentation()

        if (!emailValidator.isValid(user.username)) {
            throw InvalidUsernameException()
        }

        val existingUser = realmResource.users().search(user.username).filter { it.username == user.username }
        if (existingUser.isNotEmpty()) {
            throw UserAlreadyExistsException(UUID.fromString(existingUser[0].id), existingUser[0].username)
        }

        ur.username = user.username
        ur.email = user.username
        ur.firstName = user.firstName
        ur.lastName = user.lastName
        ur.isEnabled = true
        ur.isEmailVerified = true
        val response = realmResource.users().create(ur)

        if (response.status != 201) {
            throw Exception(response.statusInfo.reasonPhrase)
        }

        val userId = CreatedResponseUtil.getCreatedId(response)
        val userResource = realmResource.users().get(userId)
        user.userId = UUID.fromString(userId)

        userResource.roles().realmLevel().add(listOf(userRoleRep))
        try {
            userResource.executeActionsEmail(listOf("UPDATE_PASSWORD"))
        } catch (ex: Exception) { logger.error("Email for password reset couldn't be sent to user ${user.username}") }

        return user
    }

    fun updateUser(user: User) {
        user.userId?.let {
            val userResource = realmResource.users().get(it.toString())
            val userRepresentation = try {
                userResource.toRepresentation()
            } catch (e: NotFoundException) {
                throw UserNotFoundException(it)
            } catch (e: ProcessingException) {
                throw AuthServiceUnavailableException(e.message)
            }

            val emailChanged = user.username != userRepresentation.username
            userRepresentation.username = user.username
            userRepresentation.email = user.username
            userRepresentation.firstName = user.firstName
            userRepresentation.lastName = user.lastName

            if (emailChanged) {
                if (!emailValidator.isValid(user.username)) {
                    throw InvalidUsernameException()
                }
                try {
                    userResource.executeActionsEmail(listOf("VERIFY_EMAIL"))
                } catch (ex: Exception) { logger.error("Email for password verification couldn't be sent to user ${user.username}") }
            }
            userResource.update(userRepresentation)
        }
    }

    fun deleteUser(userId: UUID) {
        try {
            realmResource.users().get(userId.toString()).remove()
        } catch (e: NotFoundException) {
            throw UserNotFoundException(userId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }
    }

    fun getUserGroups(userId: UUID) =
        try {
            realmResource.users().get(userId.toString()).groups()
        } catch (e: NotFoundException) {
            throw UserNotFoundException(userId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }

    fun checkUserIsGroupAdmin(groupId: UUID, userId: UUID) {
        val adminGroupId = getGroupAdminId(groupId)
        getUserGroups(userId)
            .any { it.id == adminGroupId } || throw UserNotOrgAdminException(groupId, userId)
    }

    fun getUserOrgs(userId: UUID): List<Organization> =
        getUserGroups(userId).map {
            "/organizations/([^/]+)".toRegex().find(it.path)?.groupValues?.get(0)
        }.filterNotNull()
            .distinct()
            .map { realmResource.getGroupByPath(it).toOrg() }

    fun getKeyOrg(keyId: UUID): Organization =
        try { getUserGroups(keyId) } catch (e: UserNotFoundException) { throw KeyNotFoundException(keyId) }.first().toOrg()

    fun getLongLivedToken(username: String, pwd: String): String {
        val conn = KeycloakBuilder.builder()
            .serverUrl(url)
            .realm(realm)
            .clientId("mycrossref")
            .username(username)
            .password(pwd)
            .build()

        val token = conn.tokenManager().accessTokenString

        val response = StringBuffer()

        fun String.toLongLivedTokenResponse(): LongLivedTokenResponse = ObjectMapper().readValue(this)

        val url = URL("$url/realms/crossref/configurable-token")
        with(url.openConnection() as HttpURLConnection) {
            requestMethod = "POST" // optional default is GET
            setRequestProperty("Authorization", "Bearer $token")
            setRequestProperty("Content-type", "application/json")
            setDoOutput(true)

            val wr = OutputStreamWriter(getOutputStream())
            // 15 set years for the token lifespan , not longer can be set because
            // of limitation of integers used in the  keycloak extension
            wr.write("{\"tokenLifespanInSeconds\": 473040000}")
            wr.flush()

            BufferedReader(InputStreamReader(inputStream)).use {
                var inputLine = it.readLine()
                while (inputLine != null) {
                    response.append(inputLine)
                    inputLine = it.readLine()
                }
            }
        }

        val longLivedToken = response.toString().toLongLivedTokenResponse().access_token

        JWTParser.parse(longLivedToken).jwtClaimsSet.expirationTime < Date() && throw Exception("Error obtaining long lived token")

        conn.close()

        return longLivedToken
    }

    fun getGroupAdminId(groupId: UUID) =
        getOrgRepresentation(groupId).subGroups.filter { it.name == "admins" }[0].id

    fun getUserRepresentation(userId: UUID) =
        try {
            realmResource.users().get(userId.toString()).toRepresentation()
        } catch (e: NotFoundException) {
            throw UserNotFoundException(userId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }

    fun getUser(userId: UUID) =
        getUserRepresentation(userId).toUser()

    private fun addSubGroup(parent: GroupRepresentation, child: GroupRepresentation): UUID {
        var response: Response = realmResource.groups().add(child)
        child.id = CreatedResponseUtil.getCreatedId(response)
        response = realmResource.groups().group(parent.id).subGroup(child)
        response.close()
        return UUID.fromString(child.id)
    }
}

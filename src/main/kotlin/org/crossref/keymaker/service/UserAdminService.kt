package org.crossref.keymaker.service

import org.crossref.keymaker.api.User
import java.util.UUID

interface UserAdminService {
    fun findUsers(searchParams: Map<String, String>): List<User>

    fun addUser(user: User): User

    fun getUser(userId: UUID): User

    fun updateUser(user: User)

    fun deleteUser(userId: UUID)
}

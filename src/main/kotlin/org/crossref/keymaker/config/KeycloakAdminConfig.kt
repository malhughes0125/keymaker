package org.crossref.keymaker.config

import org.crossref.keymaker.service.impl.keycloak.KeycloakConnector
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class KeycloakAdminConfig(
    @Value("\${config.keycloak.admin.url}")
    private val url: String,
    @Value("\${config.keycloak.admin.realm}")
    private val realm: String,
    @Value("\${config.keycloak.admin.user}")
    private val user: String,
    @Value("\${config.keycloak.admin.pwd}")
    private val password: String
) {
    @Bean
    fun keycloakConnector(): KeycloakConnector = KeycloakConnector(url, realm, user, password)
}

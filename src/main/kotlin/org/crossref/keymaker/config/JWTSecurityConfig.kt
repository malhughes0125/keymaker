package org.crossref.keymaker.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.lang.exception.ExceptionUtils
import org.crossref.keymaker.api.ResponseMessage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationFilter
import java.net.ConnectException
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletResponse

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true)
class JWTSecurityConfig : WebSecurityConfigurerAdapter() {
    @Autowired
    private lateinit var mapper: ObjectMapper

    override fun configure(http: HttpSecurity) {
        http.addFilterBefore(authenticationExceptionHandlerFilter(), BearerTokenAuthenticationFilter::class.java)
            .authorizeRequests()
            .antMatchers("/actuator/**", "/v3/api-docs/**").permitAll()
            .anyRequest().authenticated()
            .and()
            .oauth2ResourceServer()
            .jwt { jwt -> jwt.jwtAuthenticationConverter(jwtAuthenticationConverter()) }
    }

    private fun jwtAuthenticationConverter(): Converter<Jwt, AbstractAuthenticationToken> =
        JwtAuthenticationConverter().apply { setJwtGrantedAuthoritiesConverter { jwt -> realmRoleConverter(jwt) } }

    @Suppress("UNCHECKED_CAST")
    private fun realmRoleConverter(jwt: Jwt): Collection<GrantedAuthority>? =
        (jwt.claims["realm_access"] as Map<String, List<String>>)["roles"]?.map { roleName ->
            SimpleGrantedAuthority(
                // It is assumed that role names are already prefixed with "ROLE_"
                roleName
            )
        }?.toList()

    private fun authenticationExceptionHandlerFilter(): Filter =
        Filter { request: ServletRequest, response: ServletResponse, chain: FilterChain ->
            try {
                chain.doFilter(request, response)
            } catch (exception: AuthenticationException) {
                response as HttpServletResponse
                val responseMessage = if (ExceptionUtils.getRootCause(exception) is ConnectException) {
                    ResponseMessage(
                        HttpStatus.SERVICE_UNAVAILABLE,
                        "Authentication service unavailable",
                        listOf(exception.message)
                    )
                } else {
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client could not be authenticated", listOf(exception.message)
                    )
                }
                response.status = responseMessage.status.value()
                response.contentType = MediaType.APPLICATION_JSON_VALUE
                response.outputStream.println(mapper.writeValueAsString(responseMessage))
            }
        }
}

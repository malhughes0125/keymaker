package org.crossref.keymaker.api

import org.springframework.http.HttpStatus
import java.time.LocalDateTime
import java.util.UUID

enum class Subscription {
    PLUS
}

data class Organization(
    var orgId: UUID?,
    val name: String,
    val sugarId: Long,
    val contact: String,
    val description: String?,
    val subscriptions: MutableList<Subscription> = mutableListOf()
)

data class User(
    var userId: UUID?,
    val username: String,
    val firstName: String?,
    val lastName: String?
)

data class Key(
    var keyId: UUID,
    val description: String,
    val issuedBy: UUID,
    val date: LocalDateTime = LocalDateTime.now(),
    val enabled: Boolean = true
)

data class KeyToken(val key: Key, val token: String)

data class ResponseMessage(
    val status: HttpStatus,
    val message: String,
    val errors: List<String?>
)

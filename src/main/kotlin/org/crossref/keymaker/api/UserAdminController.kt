package org.crossref.keymaker.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.crossref.keymaker.service.UserAdminService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import java.util.UUID
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/keymaker/api/v1/user")
class UserAdminController(
    val userAdminService: UserAdminService
) {
    @Operation(summary = "Get all users")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "Found users",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = User::class))))) ]
            ),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
        ]
    )
    @GetMapping
    @RolesAllowed("STAFF")
    fun findUsers(@RequestParam searchParams: Map<String, String>): List<User> =
        userAdminService.findUsers(searchParams)

    @Operation(summary = "Add user")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "User created",
                content = [ (Content(mediaType = "application/json", schema = Schema(implementation = User::class))) ]
            ),
            ApiResponse(responseCode = "400", description = "Bad request", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))]),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "409", description = "User already exists", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))]),
        ]
    )
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.CREATED)
    fun addUser(@RequestBody user: User): User = userAdminService.addUser(user)

    @Operation(summary = "Get user")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "User found",
                content = [ (Content(mediaType = "application/json", schema = Schema(implementation = User::class))) ]
            ),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "User not found request", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))]),
        ]
    )
    @GetMapping("/{userId}")
    @RolesAllowed("STAFF")
    fun getUser(@PathVariable userId: UUID): User = userAdminService.getUser(userId)

    @Operation(summary = "Update user")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "User updated"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "User not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))]),
        ]
    )
    @PutMapping("/{userId}")
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun updateUser(@PathVariable userId: UUID, @RequestBody user: User) =
        userAdminService.updateUser(user.apply { this.userId = userId })

    @Operation(summary = "Delete user")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "User deleted"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "User not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))]),
        ]
    )
    @DeleteMapping("/{userId}")
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteUser(@PathVariable userId: UUID) = userAdminService.deleteUser(userId)
}

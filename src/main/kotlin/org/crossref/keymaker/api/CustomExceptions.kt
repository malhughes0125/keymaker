package org.crossref.keymaker.api

import java.util.UUID

class AuthServiceUnavailableException(connectExceptionMessage: String?) : Exception(connectExceptionMessage)

class OrgNotFoundException(val orgId: UUID) : Exception()

class OrgAlreadyExistsException(val orgId: UUID, val name: String, val sugarId: Long) : Exception()

class UserAlreadyExistsException(val userId: UUID, val username: String) : Exception()

class UserNotFoundException(val userId: UUID) : Exception()

class KeyNotFoundException(val keyId: UUID) : Exception()

class InvalidUsernameException : Exception()

class InvalidOrgNameException : Exception()

class UserAlreadyOrgAdminException(val orgId: UUID, val userId: UUID) : Exception()

class UserNotOrgAdminException(val orgId: UUID, val userId: UUID) : Exception()

class OrgSubscriptionUnchangedException(val orgId: UUID, val subscription: Subscription, val status: Boolean) :
    Exception()

class KeyStatusUnchangedException(val keyId: UUID, val status: Boolean) :
    Exception()
